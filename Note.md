nvp : ...

# Ordre d'écriture pour le rendu

- 1 sommaire
- 2 remerciements
- 3 résumé en anglais
- 4 liste des compétence du référentiel
- 5 Cahier des charges ou autre trucs
- 6 Spécifications fonctionnelles
- 7 Spécifications technique
- 8 réalisation
- 9 conclusion

## introduction 

  - 1 Pour l'idée du projet s'est moi, qui est eu l'idée, mais mont idée de basse a beaucoup changé, à la besse s'était une application pour les photographes et les touristes qui veulent chercher découvrire des lieux pour réalisé des photos.
  - 2 Pour le groupe qui est composé de Aymeric(moi), Camille et de Karime, le groupe s'est formé part affinité, j'ai déja eu l'occasion de travailler avec Camille sur un TP.
  - 3 [?]
  -
  - 4 les parties que j'ai préféré faire, 
    - **Développement du Projet**
    - le recherche d'idée pour l'utilisation de l'app
      - l'utilisation de pions pour séparer les points intérêts | couleur | logo pions 
      - génération random d'un itinéraire
      - rajouté des pions dans la page filtre
      - la posibilité de clicket sur un pions **!!!!!!**
      - **(je doit aussi dire mes idées qu'on n'a pas mit sur le projet ??? )** 
    - le maquettage 
    - dévelopement du figma
    - recherche du nom et du logo 
    - Kanban sur GitHub (même si on n'a plus accès), gestion des tickets et mise en place des règles sur le nomage et "parramétrage" des tickets
    - **CODE**
    - la recherche de BUG sur la configuration du projet (mal configuré au début du projet, pressé part le temps)
    - recherche de BUG en tout genre, trouver l'origine et essayer de le résoudre 
    - Animation sur les FAB, utilisation de Native Base mais finalement fait à la main
    - la recherche sur l'utilisation de l'API de Google Map (sans succée) 
    - Git-Flow en équipe, assister mes collègues au début du projet sur l'usilisation de Git-Flow (plutot une bonne métrise de ma part)
    - Algo sur la génération random du tableau de pions

  - 5 les difficulté rencontrés
    - le MCD (le reprendre à plusieur fois, plusieur plusieur fois ) et difficile pour moi
    - trop d'idée pour l'application et donc trop ambitieux, on a du faire des restrictions et même lors du codage.
    - le maquettage d'une application Mobile (nous avons eu du mal à bien se procheter sur la maquette, si les boutons on une bonne taile ou si cette fonctionalité serra utilisable sur Mobile. Vue surtout sur les dernière mois de la formation)
    - Changement de React à React-Native, donc reconfiguration du projet "Front" (maquette)
    - sur la configuration du projet, l'utilisation de React-Native et les changements de technos que sa implique  
    - devoir faire plus de recherche pour l'utilisation de React-Native, des petits changement mais changement quand même
    - les idées qui change en cour de routes ou l'équipe qui commence avoirs des idées un peu différente sur l'utilisation de certaines fonctionnalités

  - 6 les partie ou je suis le fière 
    - le nom et le disigne du Logo
    - la recherche du BUG de storyboock qui fessait cracher l'App (génération de Storyboock dans "metro.config" et utilisation de Storyboock dans App) 
    - la recherche du BUG de latence au début du projet (cause: tsconfig mal configuré)
    - FAB, Floting Action Button au début utilisation avec Native Base, mais a cause de problème générait avec cette libréry nous avons du la supprimer, en cherchant je n'est pas trouver d'autre Composants qui me permettait de le modifier comme je le veut, donc casse la tienne, je l'ai refait à la main grâçe à une vidéo
    - l'Algo du rendom TableauPions ou je me suis fixer la matiné pour le terminé et je suis arrivé dans les temps

  - 7 les partis ou je souhaite des améliorations 
    - sur la maquette Figma, avec du recul, je trouve que la maquette est pas dutout pensé pour le Mobile mais plus comme une trensition entre le Web => Mobile, après utilisation du prototype et des comparaison sur d'autre application, je comprend un peu plus les choix fait sur les autres application (avec nos habitude d'utilisation)
    - une configuration propre et bien travailler et pas un truc fait à l'arrache, faire une sorte pré-configuration de test, pour tester expo, Storyboock, library, Jest, Detox, Eslint, Prettier, etc, pour avoir une base solide
    - et plein de trucs à améliorer au niveau du code

- nos conditions pour réaliser le projet **Date!!!**
  - temps **!?!?!?!**
    - deux semaine pour réalisé la mise en place du projet, l'utilité de l'application, MCD, persona, maquettage, cahier des charges, userJourné, 
    - trois semaine de code
      - semaine 3: préparation pour le Devops = Storybook et Docker/Render  
      - semaine 4: correction config
      - semaine 5: codage
      - semaine 6: codage

  - ressources 
    - React-Native
    - Expo 
    - Node
    - GitHub => GitLab
    - Kanban : GitHub => GitLab

  - compétences
    - ???

  - planning
    - sprint d'une semaine avec (rotation du leader lors du codage)

  - cour en lien avec le projet
    - une liste à faire mais la flemme de la faire toute suite (et surtout j'ai plus GitHub)

### blablablabla

Le lancement de la première semaine du projet était plutôt bien parti, même si c'était compliqué de bien se projeter sur notre future application, sur le fait qu’il fallait revoir l’utilité du projet,  et à qui l’application pourrait être utile. 
Donc nous avons dû revoir la conception, les Personas, le wireframe et le fait que nous sommes trois sur le projet et non deux, et donc trouver des solutions pour bien se répartir les tâches. 
Nous avons commencé par réaliser un Gantt pour essayer de bien se répartir les missions, choix d’un Kanban et la mise en place de celui ci (même si on l'a utilisé surtout lors du code),
Après notre première réunion pour re-définir l’utilité de notre application, nous avons commencé à travailler sur les premières briques de notre projet. J’ai eu la tâche de réaliser le wireframe avec les nouvelles idées du projet, l’idée de base était une application Mobile, mais lors de la formation, nous n'avons jamais codé en format Mobile, donc par peur, nous avons fait le choix de partir sur une version Web en React.
Dans la semaine 2 notre formateur référent, nous a bien confirmé que c'était faisable de coder une application Mobile avec React-Native, de plus que nous allons avoir droit à un cours sur cette techno.
Donc, j’ai du refaire un wireframe en version Mobile et c’est là qu’on n’a commencé à voire les problèmes et les différences, pour concevoir une Appli Web ou Mobile, autre chose que j’ai remarqué lors de la création du wireframe et le début de la maquette, s’est que s’est compliqué de rassembler tous les idées et surtout la vision peut énormément changer entre les personnes.

Mais aussi les multiples changements au niveau du Footer et du header ou il y a eu énormément changement, placement des boutons, le rajout des FAB, (plutard au niveau du code le changement du bouton Random en refresh et ajout du bouton Random).

Pour le reste des semaines nous avons continué à travailler sur le MCD, maquette, personas, Gantt, User Journeys et  le cahier des charges.

