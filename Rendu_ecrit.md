# Sommaire

- (a construire plutard)

# Résumé en anglais

- bah s'est le résumé en Français mais en Anglais



## Conception du Projet

### première idée et évolutions du projet 

- un résumé à faire 

(Comment l'idée du projet a-t-elle été trifouillée jusqu'à devenir le projet final ?
Expliquez le plus précisément tout ce qui doit être fait!)

### Personas et user journeys

(Copier coller et on explique ce que c'est et ce que ça a apporté)

### Wireframes et maquettes

- Wireframe

  explique le début du Wireframe et la "représantation" que t'avais au début et le faite que t'es aller un peu plus loin sur le Wireframe

- maquette

  explique les changements qu'on a du faire sur la maquette et les rechangement de la maquette au file des mois 

  sur la maquette Figma, avec du recul, je trouve que la maquette est pas dutout pensé pour le Mobile mais plus comme une trensition entre le Web => Mobile, après utilisation du prototype et des comparaison sur d'autre application, je comprend un peu plus les choix fait sur les autres application (avec nos habitude d'utilisation)

(Exemples et modifications apportées + maquettes + choix des couleurs)





## Spécifications fonctionnelles

### site map

  - le truc avec le plan d'intégration
  - une explication vite fais bien fait du site ou Mobile

(On colle l'image si vous l'avez sinon tant pis...)

### MCD

  - une révision s'impose lors de la relecture du MCD, "donc fait gaffe"

(On colle le MCD mais surtout on explique les relations, les tables, les champs...)

### Gestion de projet (Diagramme de Gantt et ou Kanban)

  - pour le Kanban tu m'est l'image de celuis de GitHub et après celuis de GitLab
  - reprend ce que tu as fait sur le rendu Devopos, s'est du copier coler juste le changement de GitHub => GitLab  

(On colle et on explique le prévisionnel et comment on a prévu de gérer le projet dans le cas idéal prévu en début de projet)




## Spécifications techniques

### les technologies

  - une liste des technos que vous avez utilisé sur le projet, Front et Back

(React, Node, Java, PostgreSQL, C#, C++ surtout !!! etc... avec quelques logos svp ou une liste)

### l’architecture

  - pas vraiment comprit ce qui faut faire **?????**

(Un beau schema avec des nuages, des carrés, des flèches et des couleurs
Miro peut aider ou si vous avez mieux => dans le channel adéquat)

## Réalisation du projet

  -  on parle de ce qu'on n'a pas pu validé dans le projet ????

(A vous de parler de ce que vous voulez. Regardez les compétences à valider et parler de celles que vous n'avez pas encore validées ^^)

### Développement

  - des exemples de plein de trucs avec le projet **qui à permit de dévellopper l'application** **?????**

(Du code, des commits, des graphs de branches, des PR, des issues, des bugs, des features, des tests, des déploiements, des rollback, des merges, des conflits, des reviews, des refactors)

### Améliorations et difficultés rencontrées dans le travail d'équipe

  - explique les "trucs" difficulté que tu as rancontré avec ton équipe (ex audébut: Git-Flow et les problèmes de "destributions" de code "pas très serein" sur la récupération de code lors rebase/comflit )

Pareil :
(Du code, des commits, des graphs de branches, des PR, des issues, des bugs, des features, des tests, des déploiements, des rollback, des merges, des conflits, des reviews, des refactors)

### Etapes / Avancées principales du projet

  - les étapes suivente pour continuer le projet (ex: l'API de Google et un Algo pour générer un "vrai" itinéraire)

Pareil :
(Du code, des commits, des graphs de branches, des PR, des issues, des bugs, des features, des tests, des déploiements, des rollback, des merges, des conflits, des reviews, des refactors)





## Conclusion

Estimez-vous avoir atteint l'objectif du projet ?
Qu'est-ce que vous auriez aimé faire de plus dans ce projet ?

Qu'avez-vous appris en réalisant ce projet ?
Qu'avez-vous appris sur vous-même en réalisant ce projet ?
Qu'avez-vous appris sur les autres en réalisant ce projet ?
Qu'avez-vous appris sur le travail en groupe en réalisant ce projet ?
Qu'avez-vous appris sur les différents métiers du numérique en réalisant ce projet ?

A quel point êtes vous fier de votre travail ?
Résumez votre situation en début d'année et comparez la à votre situation actuelle.
Quel est votre prochain objectif après ce projet ?

Trouvez une phrase qui résume votre projet professionnel et personnel (présent ou futur).
